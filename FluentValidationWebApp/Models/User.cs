﻿namespace FluentValidationWebApp.Models
{
    public class User
    {
        public string Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SIN { get; set; }
    }
}
