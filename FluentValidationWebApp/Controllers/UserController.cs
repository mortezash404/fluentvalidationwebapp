﻿using FluentValidationWebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace FluentValidationWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpPost]
        public IActionResult Post(User user)
        {
            return NoContent();
        }
    }
}